# Tab flip for [Tree Style Tab][TST]

Uses [Tree Style Tab API][TST API] so that clicking the active tab switch to the last active one.
You can also use Ctrl+Shift+Space to trigger this behavior.

It has been tested to work well with [Multiple Tab Handler][MTH]; mostly, it uses a bit the
[Multiple Tab Handler API][MTH API] to ensure a consistent behavior with selections.

Inspiration comes from the (now dead) [Tab Tree][Tab Tree] addon.

This addon is released under LGPL-3.0 or later; the [icon][icon] is from Breeze and released under LGPL-3.0.

[TST]: https://addons.mozilla.org/firefox/addon/tree-style-tab/
[TST API]: https://github.com/piroor/treestyletab/wiki/API-for-other-addons
[MTH]: https://addons.mozilla.org/firefox/addon/multiple-tab-handler/
[MTH API]: https://github.com/piroor/multipletab/wiki/API-for-other-addons
[Tab Tree]: https://addons.mozilla.org/firefox/addon/tab-tree/
[icon]: https://github.com/KDE/breeze-icons/blob/master/icons/actions/16/mail-forwarded-replied.svg
